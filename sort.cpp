//implementa��o dos algoritimos de ordena��o
#include "sort.hpp"

void bubble(int* v, int n)
{
	int i, j, troca;
	int aux;

	for (i = 0; i < n - 1; i++)
	{
		troca = 0;
		for (j = 1; j < n - i; j++)
		{
			if (v[j] < v[j - 1])
			{
				aux = v[j];
				v[j] = v[j - 1];
				v[j - 1] = aux;
				troca++;
			}
		}
		if (troca == 0)
			break;
	}
}

void selection(int* v, int n)
{
	int i, j, Min;
	int aux;
	for (i = 0; i < n - 1; i++)
	{
		Min = i;
		for (j = i + 1; j < n; j++)
		{
			if (v[j] < v[Min])
				Min = j;
		}
		if (i != Min)
		{
			aux = v[Min];
			v[Min] = v[i];
			v[i] = aux;
		}
	}
}

void insertion(int* v, int n)
{
	int i, j;
	for (i = n - 2; i >= 0; i--)
	{
		v[n] = v[i];
		j = 1 + 1;
		while (v[n] > v[j])
		{
			v[j - 1] = v[j];
			j++;
		}
		v[j - 1] = v[n];
	}
}

void shellSort(int* v, int n)
{
	int i, j, h;
	int aux;

	for(h = 1; h < n; h = 3 * h + 1);//h inicial

	do
	{
		h = (h - 1) / 3; // atualiza o h

		for(i = h; i < n; i++)
		{
			aux = v[i];
			j = 1;

			/* comparaçoes entre os elementos de distancia de h*/
			while (v[ j- h] > aux)
			{
				v[j] = v[j - h];
				j -= h;
				if( j < h)
					break;
			}
			v[j] = aux;
		}
	} while (h != 1);
	
}

//QuickSort
//ordena o vetor v[0...n-1]
void QuickSort(int* v, int n)
{
	QuickSort_orderna(v, 0,  n - 1);
}

//ordena o vetor v[esqueda...direita]
void QuickSort_orderna(int* v, int esq, int dir)
{
	int i, j;

	QuickSort_particao(v, esq, dir, &i, &j);
	if(esq < j)
		QuickSort_orderna(v, esq, j);
	if(i < dir)
		QuickSort_orderna(v, i, dir);
}

void QuickSort_particao(int* v, int esq, int dir, int* i, int* j)
{
	int pivo, aux;
	*i = esq;
	*j = dir;

	pivo = v[(*i + *j) / 2]; //obtem o pivo x

	do
	{
		while(!(pivo <= v[*i]))
			(*i)++;
		while(pivo < v[*j])
			(*j)--;

		if( *i <= *j)
		{
			aux = v[*i];
			v[*i] = v[*j];
			v[*j] = aux;

			(*i)++;
			(*j)--;
		}
	}while(*i <= *j);
}