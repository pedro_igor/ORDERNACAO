#ifndef MERGE_HPP
#define MERGE_HPP

#include <iostream>
#include <cstdlib>
#include <cstdio>

void mergeSort(int* v, int n);
void mergeSort_ordena(int* v, int esq, int dir);
void mergeSort_intercala(int* v, int esq, int meio, int dir);
void mergeSort_interativo(int* v, int n);

#endif // !MERGE_HPP

