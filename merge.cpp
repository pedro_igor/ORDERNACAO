#include "merge.hpp"

// Orderna o vetor v[0..n-1]
void mergeSort(int* v, int n)
{
	mergeSort_ordena(v, 0, n - 1);
}

// Orderna o vetor v[esq...dir]
void mergeSort_ordena(int* v, int esq, int dir)
{
	if (esq >= dir)
		return;

	int meio = (esq + dir) / 2;
	mergeSort_ordena(v, esq, meio);
	mergeSort_ordena(v, meio + 1, dir);
	mergeSort_intercala(v, esq, meio, dir);
}

// Inteercala os vetores v[esq..meio] e v[meio + 1 .. dir]
void mergeSort_intercala(int* v, int esq, int meio, int dir)
{
	int i, j, k;

	int a_tam = meio - esq + 1;
	int b_tam = dir - meio;

	int* a = (int*)malloc(sizeof(int) * a_tam);
	int* b = (int*)malloc(sizeof(int) * b_tam);

	for (i = 0; i < a_tam; i++)
		a[i] = v[i + esq];

	for (i = 0; i < b_tam; i++)
		b[i] = v[i + meio + 1];

	for (i = 0, j = 0, k = esq; k <= dir; k++)
	{
		if (i == a_tam)
			v[k] = b[j++];
		else if (j == b_tam)
			v[k] = a[i++];
		else if (a[i] < b[j])
			v[k] = a[i++];
		else
			v[k] = b[j++];
	}

	free(a);
	free(b);
}

// Ordena o vetor v[0..n-1]
// A cada intera��o, tntercala dois "blocos" de b elementos:
// - o primeiro com o segundo, o Terceiro com o quarto etc.
void mergeSort_interativo(int* v, int n)
{
	int esq, dir;
	int b = 1;
	while (b < n)
	{
		esq = 0;
		while (esq + b < n)
		{
			dir = esq + 2 * b;
			if (dir > n)
				dir = n;
			mergeSort_intercala(v, esq, esq + b - 1, dir - 1);
			esq = esq + 2 * b;
		}
		b *= 2;
	}
}