//algoritimos de orderna��o

#ifndef SORT_HPP
#define SORT_HPP

#include <iostream>
#include <cstdlib>

using namespace std;

void bubble(int* v, int n);
void selection(int* v, int n);
void insertion(int* v, int n);
void shellSort(int* v, int n);

//QuickSort
//ordena o vetor v[0...n-1]
void QuickSort(int* v, int n);
//ordena o vetor v[esqueda...direita]
void QuickSort_orderna(int* v, int esq, int dir);
void QuickSort_particao(int* v, int esq, int dir, int* i, int* j);
#endif // !SORT_HPP
